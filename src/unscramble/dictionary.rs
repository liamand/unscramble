use std::str;
use std::fs;

use std::collections::HashMap;

use super::constants;

pub fn load_dictionary(file_path: &str) -> HashMap<String, i32> {
    let mut dictionary: HashMap<String, i32> = HashMap::new();

    for line in fs::read_to_string(file_path).expect(constants::FILE_MISSING).lines() {
        dictionary.insert(line.to_lowercase().to_string(), 1);
    }

    return dictionary;
}