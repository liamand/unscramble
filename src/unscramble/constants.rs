pub const ALPHABET: &str = "abcdefghijklmnopqrstuvwxyz";

pub const MAX_LETTERS: i32 = 8;
pub const MIN_LETTERS: i32 = 2;

pub const FILE_MISSING: &str = "File is missing, please ensure file is available";