use std::collections::{HashMap, hash_map::Entry};
use std::env;
use std::io;

use regex::Regex;
use lazy_static::lazy_static;

use super::constants;

pub fn split_string(input: &str) -> Vec<String> {
    let mut split: Vec<String> = input.split("").map(str::to_string).collect();

    split.remove(0);
    split.remove(split.len() - 1);

    return split;
}

pub fn sort_output(word_list: HashMap<String, i32>) -> HashMap<i32, Vec<String>>{
    let mut sorted_output: HashMap<i32, Vec<String>> = HashMap::new();

    for (key, _) in word_list {
        let length = key.len() as i32;
        match sorted_output.entry(length) {
            Entry::Vacant(e) => {
                let mut vec: Vec<String> = Vec::new();
                vec.push(key);
                e.insert(vec);
            },
            Entry::Occupied(mut e) => {
                e.get_mut().push(key);
            },
        }
    }

    return sorted_output;
}

fn word_lengths_desc(sorted_word_list: &HashMap<i32, Vec<String>>) -> Vec<i32> {
    let mut word_lengths: Vec<i32> = Vec::new();

    for (key, _) in sorted_word_list {
        word_lengths.push(*key);
    }

    word_lengths.sort();
    word_lengths.reverse();

    return word_lengths;
}

pub fn sorted_print(sorted_word_list: HashMap<i32, Vec<String>>) {
    let word_lengths_desc: Vec<i32> = word_lengths_desc(&sorted_word_list);

    for key in word_lengths_desc {
        let res: Option<&Vec<String>> = sorted_word_list.get(&key);

        match res {
            Some(vec) => {
                println!("[{key}]:");
                for sub_list in vec.chunks(10) {
                    let word_list_string: String = sub_list.join(", ");
                    println!("{word_list_string}");
                }
                println!();
            },
            None => {
                println!("An extra key ended up here somehow.");
                return;
            },
        }
    }
}

pub fn word_input() -> Option<String> {
    lazy_static! {
        static ref RE: Regex = Regex::new(r"([a-zA-Z])").expect("Regex broke");
    }

    let max: i32 = constants::MAX_LETTERS;
    let min: i32 = constants::MIN_LETTERS;
    println!("Enter up to {max} scrambled letters: [press Ctrl-C to quit]");

    let mut scrambled_letters: String = String::new();
    io::stdin().read_line(&mut scrambled_letters).expect("something broke");

    if env::consts::OS == "windows" {
        scrambled_letters.pop();
        scrambled_letters.pop();
    } else {
        scrambled_letters.pop();
    }

    if scrambled_letters.len() == 1 {
        println!("You must enter at least {min} or at most {max} letters.");
        return None;
    } else if scrambled_letters.len() as i32 > constants::MAX_LETTERS || scrambled_letters.len() as i32 == constants::MIN_LETTERS {
        println!("You must enter at least {min} or at most {max} letters.");
        return None;
    } else if scrambled_letters.matches("?").count() > 2 {
        println!("Maximum 2 wildcards.\n");
        return None;
    } else if !RE.is_match(&scrambled_letters) {
        println!("Only 'a-z', and two max wildcards ('?') allowed.\n");
        return None;
    }

    return Some(scrambled_letters);
}