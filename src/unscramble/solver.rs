use std::collections::HashMap;

use super::{constants, utils};

struct Argument {
    word: String,
    letters: Vec<String>
}

pub fn solve(letters: String, dictionary: &HashMap<String, i32>) -> HashMap<String, i32> {
    // Create empty hashmap for strings 
    let mut words_found: HashMap<String, i32> = HashMap::new();

    let mut arguments: Vec<Argument> = Vec::new();
    arguments.push(
        Argument { 
            word: String::from(""), 
            letters: utils::split_string(letters.as_str()) 
        }
    );

    while arguments.len() > 0 {
        let res: Option<Argument> = arguments.pop();

        match res {
            Some(argument) => {
                let Argument {word, letters} = argument;


                if dictionary.contains_key(&word) {
                    words_found.insert(word.clone(), 1);
                }

                for i in 0..letters.len() {
                    let mut new_letters: Vec<String> = letters.clone();
                    new_letters.remove(i);

                    let current_letter: String = String::from(&letters[i]);
                    let is_wild_card: bool = &current_letter == "?";

                    if is_wild_card {
                        let alphabet: Vec<String> = utils::split_string(constants::ALPHABET);
                        for j in 0..alphabet.len() {
                            arguments.push(
                                Argument { 
                                    word: word.clone() + &alphabet[j].to_string(), 
                                    letters: new_letters.clone() 
                                }
                            )
                        }
                    } else {
                        arguments.push(
                            Argument { 
                                word: word.clone() + &current_letter.to_string(), 
                                letters:  new_letters.clone()
                            }
                        )
                    }

                }
            },
            None => {
                return words_found;
            }
        }

    }

    return words_found;
}