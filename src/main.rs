mod unscramble;

// const DICTIONARY: &str = "/home/liam/code/word-unscramble/unscramble/dict";

fn main() {
    let dictionary = unscramble::dictionary::load_dictionary("dict");

    loop {
        let res = unscramble::utils::word_input();
        println!();

        match res {
            Some(letters) => {
                let output = unscramble::solver::solve(letters, &dictionary);
                let sorted_output = unscramble::utils::sort_output(output);

                unscramble::utils::sorted_print(sorted_output);
            },
            None => {
                
            },
        }
    }
}
